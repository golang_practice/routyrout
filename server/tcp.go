package server

import (
	"bufio"
	"errors"
	"fmt"
	"net"
	"strings"

	log "github.com/sirupsen/logrus"
)

// TCPServer holds the struct of our TCP implementation
type TCPServer struct {
	addr    string
	server  net.Listener
	servers []string
}

// Run starts the TCP Server and spawns thread for new connections
func (t *TCPServer) Run() (err error) {
	t.server, err = net.Listen("tcp", t.addr)
	if err != nil {
		return err
	}
	defer t.Close()
	log.Info("Listening for connections on: ", t.addr+"...")

	for {
		conn, err := t.server.Accept()
		if err != nil {
			err = errors.New("Could not accept connection " + conn.RemoteAddr().String())
			break
		}
		if conn == nil {
			err = errors.New("Could not create connection")
			break
		}
		log.Info("New connection from: ", conn.RemoteAddr().String())
		go t.handleConnection(conn)
	}
	return
}

// Close shuts down the TCP Server
func (t *TCPServer) Close() (err error) {
	return t.server.Close()
}

func (t *TCPServer) determineConnRequest(request string) (string, error) {
	switch strings.ToLower(request) {
	case "send":
		return "send", nil
	}
	return "", errors.New("Invalid Request Type")
}

func (t *TCPServer) forward(conn net.Conn) {
	rw := bufio.NewReadWriter(bufio.NewReader(conn), bufio.NewWriter(conn))

	for {
		data, err := rw.ReadString('\n')
		if err != nil {
			rw.WriteString("Failed to understand data")
			rw.Flush()
			return
		}
		if data == "localhost:2345" {

		}
	}
}

func (t *TCPServer) handleConnection(conn net.Conn) error {
	defer conn.Close()

	rw := bufio.NewReadWriter(bufio.NewReader(conn), bufio.NewWriter(conn))
	for {
		data, err := rw.ReadString('\n')

		if err != nil {
			rw.WriteString("Failed to read input")
			rw.Flush()
			return err
		}
		if data == "forward" {
			t.forward(conn)
		}
		rw.WriteString(fmt.Sprintf("Request recv: %s", data))
		rw.Flush()
	}
}
