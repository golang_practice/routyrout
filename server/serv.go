package server

import (
	"errors"
	"strings"
)

// Server dinefines the minimum contract for our servers
type Server interface {
	Run() error
	Close() error
}

// NewServer creates a new Server given protocol and address
func NewServer(protocol, addr string) (Server, error) {
	switch strings.ToLower(protocol) {
	case "tcp":
		return &TCPServer{
			addr:    addr,
			servers: []string{"localhost:2345"},
		}, nil
	}

	return nil, errors.New("Invalid protocol")
}

func (s *Server) findServer() string {

}
