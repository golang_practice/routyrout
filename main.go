package main

import (
	"flag"
	"strconv"

	"gitlab.com/sean__ch0n/routyrout/server"
)

func main() {
	addr := flag.String("addr", "localhost", "bind ip")
	port := flag.Int("port", 5555, "bind port")
	flag.Parse()

	servStr := *addr + ":" + strconv.Itoa(*port)
	tcpServ, err := server.NewServer("tcp", servStr)
	if err != nil {
		panic(err)
	}
	tcpServ.Run()
}
